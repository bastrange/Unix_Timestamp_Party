#Requirements for each medium

	Title(?):
	
	UnixTimeParty(ANewEpoch)

	var ANewEpoch = 1.5M
	
**General Poster:**

- Title of the party
- Time in local time and in Unix Time
- Visual-like references to what unix time is
- link to git
- link to website
- Participators 
- Host locations

**Handout:**

- Title of the party
- Time in local time and in Unix Time
- short text about unix time
- git
- Link to website

**Invitation:**

- Title of Party
- Time in local time (Ams) and in Unix Time
- short text about unix time (abbreviation)
- Host locations
- Link to website

**Website:**

- Realtime Unix timecode
- Realtime Binary timecode
- Realtime Local time
- Multiple backgrounds / video
- Title of the Party
- short text about unix time
- Link to git